package com.example.lenovo.stock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends BaseAdapter {

    private Context context;
    private List<Product> products;

    public ListAdapter(Context context, List<Product> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Product getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if (rowView == null) {
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.name.setText(getItem(position).getName());
        holder.onStock.setText(getItem(position).getOnStock()+"");
        return rowView;
    }

    static class ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.onStock)
        TextView onStock;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }

    public void addItem(Product product) {
        products.add(product);
        notifyDataSetChanged();
    }

    public void buyItem(int id){
        if(products.get(id).getOnStock() > 0){
            products.get(id).decrementOnStock();
            notifyDataSetChanged();
        }
    }
}
