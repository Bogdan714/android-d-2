package com.example.lenovo.stock;

public class Product {
    String name;
    int onStock;

    public Product(String name, int onStock) {
        this.name = name;
        this.onStock = onStock;
    }

    public String getName() {
        return name;
    }

    public int getOnStock() {
        return onStock;
    }

    public void decrementOnStock() {
        this.onStock--;
    }
}
