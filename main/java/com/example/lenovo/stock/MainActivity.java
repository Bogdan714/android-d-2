package com.example.lenovo.stock;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    boolean paused = true;
    int productsAmount = 5;

    ListAdapter adapter;
    Thread threadOfClients;
    Thread worker;
    Deque<Integer> queue = new ArrayDeque<>();

    @BindView(R.id.pause)
    Button pause;
    @BindView(R.id.queue)
    TextView clientsBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        pause.setText(paused?"go":"pause");
        adapter = new ListAdapter(this, generateProducts(productsAmount));
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(adapter);

        @SuppressLint("HandlerLeak") final Handler clientBarHandler = new Handler() {
            public void handleMessage(Message msg) {
                clientsBar.setText(msg.what + "\nQueue");
            }
        };

        @SuppressLint("HandlerLeak") final Handler transactionHandler = new Handler() {
            public void handleMessage(Message msg) {
                adapter.buyItem(msg.what);
            }
        };

        Runnable ordering = new Runnable() {
            public void run() {
                while (true) {
                    int wish = (int) (Math.random() * productsAmount);
                    queue.add(wish);
                    clientBarHandler.sendEmptyMessage(queue.size());
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        Runnable service = new Runnable() {
            public void run() {
                while (true) {

                    if (!paused && queue.size() > 0) {
                        int order = queue.poll();
                        transactionHandler.sendEmptyMessage(order);
                        clientBarHandler.sendEmptyMessage(queue.size());
                    }

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        clientsBar.setText(String.valueOf(queue.size()));
        threadOfClients = new Thread(ordering);
        worker = new Thread(service);
        threadOfClients.start();
        worker.start();
    }

    @OnClick(R.id.pause)
    public void onClick() {
        if (paused) {
            pause.setText("pause");
        } else {
            pause.setText("go");
        }
        paused = !paused;
    }

    public List<Product> generateProducts(int amount) {
        List<Product> list = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            list.add(new Product("product " + i, 10));
        }
        return list;
    }


}

